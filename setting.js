var Setting = {
	preload(){
		// this.game.load.image('title', './assets/button.png');
		this.game.load.image('button1', './assets/title.png');
		this.game.load.image('volume', './assets/volume.jpg');
	},

	create(){
		this.game.add.image(0, 0, 'settingBg');
		this.hoverSound = this.game.add.audio('hover');
		// var title = this.game.add.image(200, 50, 'title');
		// title.scale.setTo(0.5, 0.5);
		// title.anchor.setTo(0.5, 0.5);

		this.game.camera.flash(0xffffff, 300);

		var back = this.game.add.button(200, 600, 'button1', ()=>{
			// this.game.state.start('singlePlayer');
			this.game.state.start('menu');
		}, this);
		back.anchor.setTo(0.5, 0.5);
		back.scale.setTo(0.3, 0.3);
		var txt1 = this.game.add.text(200, 600, 'back', {
			font: '24px font',
			fill: '#ffffff'
		});
		txt1.anchor.setTo(0.5, 0.5);
		back.alpha = 0.8;

		back.onInputOver.add(()=>{
			back.alpha = 1;

			this.hoverSound.volume = this.game.volumeSound;
			this.hoverSound.play();
		}, this);
		back.onInputOut.add(()=>{
			back.alpha = 0.8;
		}, this);




		this.volume = this.game.add.text(450, 200, 'Sound Effect', {
			font: '20px font',
			fill: '#ffffff'
		});
		this.volume.anchor.setTo(0, 0.5);

		this.volumeTxt = this.game.add.text(750, 200, '100%', {
			font: '20px font',
			fill: '#ffffff'
		});
		this.volumeTxt.anchor.setTo(1, 0.5);
		this.volumeTxt.text = Math.round(this.game.volumeSound * 100) + '%';

		this.volumeBarRec = this.game.add.graphics(0, 0);
		this.volumeBarRec.lineStyle(2, 0x555555, 5);
		this.volumeBarRec.drawRect(this.game.width/2 - 151, 229, 302, 32);
		this.volumeBarRec.anchor.setTo(0.5, 0.5);
		this.volumeBar = this.game.add.sprite(this.game.width/2 - 150, 230,'volume');
		this.volumeBar.width = this.game.volumeSound * 300;
		this.pointer = this.game.input.mousePointer;

		this.changingVolume = false;






		this.volumeMusic = this.game.add.text(450, 300, 'Music', {
			font: '20px font',
			fill: '#ffffff'
		});
		this.volumeMusic.anchor.setTo(0, 0.5);

		this.volumeTxtMusic = this.game.add.text(750, 300, '100%', {
			font: '20px font',
			fill: '#ffffff'
		});
		this.volumeTxtMusic.anchor.setTo(1, 0.5);
		this.volumeTxtMusic.text = Math.round(this.game.volumeMusic * 100) + '%';

		this.volumeBarRecMusic = this.game.add.graphics(0, 0);
		this.volumeBarRecMusic.lineStyle(2, 0x555555, 5);
		this.volumeBarRecMusic.drawRect(this.game.width/2 - 151, 329, 302, 32);
		this.volumeBarRecMusic.anchor.setTo(0.5, 0.5);
		this.volumeBarMusic = this.game.add.sprite(this.game.width/2 - 150, 330,'volume');
		this.volumeBarMusic.width = this.game.volumeMusic * 300;

		this.changingVolumeMusic = false;
	},

	update(){
		if(
			(this.pointer.x >= this.game.width/2 - 150
			&& this.pointer.x <= this.game.width/2 + 150
			&& this.pointer.y >= 230
			&& this.pointer.y <= 260)
			|| this.changingVolume === true
			){
			if(this.pointer.leftButton.isDown){
				this.volumeBar.width = Math.min(Math.max( (this.pointer.x - (this.game.width/2 - 150) ), 0), 300);
				// this.game.sound.volume = this.volumeBar.width / 300;
				this.game.volumeSound = Math.round(this.volumeBar.width / 3)/100;
				this.changingVolume = true;
				this.volumeTxt.text = Math.round(this.game.volumeSound * 100) + '%';
			}
			else {
				this.changingVolume = false;
			}
		}

		else if(
			(this.pointer.x >= this.game.width/2 - 150
			&& this.pointer.x <= this.game.width/2 + 150
			&& this.pointer.y >= 330
			&& this.pointer.y <= 360)
			|| this.changingVolumeMusic === true
			){
			if(this.pointer.leftButton.isDown){
				this.volumeBarMusic.width = Math.min(Math.max( (this.pointer.x - (this.game.width/2 - 150) ), 0), 300);
				this.game.volumeMusic = Math.round(this.volumeBarMusic.width / 3)/100;
				this.game.menuMusic.volume = this.game.volumeMusic;
				this.changingVolumeMusic = true;
				this.volumeTxtMusic.text = Math.round(this.game.volumeMusic * 100) + '%';
			}
			else {
				this.changingVolumeMusic = false;
			}
		}
	}
}


//this.game.volumeSound