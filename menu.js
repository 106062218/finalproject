var Menu = {
	preload(){
		this.game.load.image('button2', './assets/button_2.png');
		this.game.load.image('button3', './assets/button_3.png');
		this.game.load.image('button1', './assets/title_1.png');
		this.game.load.image('button_shadow', './assets/title.png');
		this.game.stage.backgroundColor = '#ff8c1a';
	},

	create(){
		this.game.add.image(0, 0, 'menuBg');
		if(this.game.gameMusic !== null){
			this.game.gameMusic.destroy();
			this.game.gameMusic = null;
		}
		if(this.game.menuMusic === null){
			this.game.menuMusic = this.game.add.audio('menuMusic');
			this.game.menuMusic.loop = true;
			this.game.menuMusic.volume = this.game.volumeMusic;
			this.game.menuMusic.play();
		}
		

		this.hoverSound = this.game.add.audio('hover');
		this.game.camera.flash(0xffffff, 500);
		var button1_s = this.game.add.image(900, 185, 'button_shadow');
		button1_s.alpha = 0;
		button1_s.anchor.setTo(0.5, 0.5);
		var button1 = this.game.add.button(900, 185, 'button1', ()=>{
			state = 1;
			this.game.state.start('maps');
		}, this);
		button1.anchor.setTo(0.5, 0.5);
		var txt1 = this.game.add.text(900, 185, '單人', {
			font: '30px font',
			fill: '#000000'
		});
		txt1.anchor.setTo(0.5, 0.5);

		var button2_s = this.game.add.image(900, 285, 'button_shadow');
		button2_s.alpha = 0;
		button2_s.anchor.setTo(0.5, 0.5);
		var button2 = this.game.add.button(900, 285, 'button1', ()=>{
			state = 2;
			this.game.state.start('maps');
		}, this);
		button2.anchor.setTo(0.5, 0.5);
		var txt2 = this.game.add.text(900, 285, '雙人', {
			font: '30px font',
			fill: '#000000'
		});
		txt2.anchor.setTo(0.5, 0.5);

		var button3_s = this.game.add.image(900, 385, 'button_shadow');
		button3_s.alpha = 0;
		button3_s.anchor.setTo(0.5, 0.5);
		var button3 = this.game.add.button(900, 385, 'button1', ()=>{
			this.game.state.start('setting');
		}, this);
		button3.anchor.setTo(0.5, 0.5);
		var txt3 = this.game.add.text(900, 385, '設定', {
			font: '30px font',
			fill: '#000000'
		});
		txt3.anchor.setTo(0.5, 0.5);

		button1.scale.setTo(0.4, 0.4);
		button2.scale.setTo(0.4, 0.4);
		button3.scale.setTo(0.4, 0.4);
		button1_s.scale.setTo(0.4, 0.4);
		button2_s.scale.setTo(0.4, 0.4);
		button3_s.scale.setTo(0.4, 0.4);

		button1.onInputOver.add(()=>{
			button1_s.alpha = 0.8;

			this.hoverSound.volume = this.game.volumeSound;
			this.hoverSound.play();
		}, this);
		button1.onInputOut.add(()=>{
			button1_s.alpha = 0;
		}, this);

		button2.onInputOver.add(()=>{
			button2_s.alpha = 0.8;

			this.hoverSound.volume = this.game.volumeSound;
			this.hoverSound.play();
		}, this);
		button2.onInputOut.add(()=>{
			button2_s.alpha = 0;
		}, this);

		button3.onInputOver.add(()=>{
			button3_s.alpha = 0.8;

			this.hoverSound.volume = this.game.volumeSound;
			this.hoverSound.play();
		}, this);
		button3.onInputOut.add(()=>{
			button3_s.alpha = 0;
		}, this);


		this.game.add.text(200, 100, '發生在部落之間的衝突', {
			font: '50px font',
			fill: '#000000'
		});
		
	},

	update(){

	}
}
var state;