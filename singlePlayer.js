var SinglePlayer = {
	preload(){
		//small dick, dont preload here
	},

	create(){
		if(this.game.mapNumber === 1){
			this.background = this.game.add.image(this.camera.view.x-50 , this.camera.view.y-50, 'background1');
		}
		else if(this.game.mapNumber === 2){
			this.background = this.game.add.image(this.camera.view.x-50 , this.camera.view.y-50, 'background2');
		}
		else if(this.game.mapNumber === 3){
			this.background = this.game.add.image(this.camera.view.x-50 , this.camera.view.y-50, 'background3');
		}
		else if(this.game.mapNumber === 4){
			this.background = this.game.add.image(this.camera.view.x-50 , this.camera.view.y-50, 'background4');
		}
		if(this.game.gameMusic !== null){
			this.game.menuMusic.destroy();
			this.game.menuMusic = null;
		}
		if(this.game.gameMusic === null){
			this.game.gameMusic = this.game.add.audio('gameMusic');
			this.game.gameMusic.loop = true;
			this.game.gameMusic.volume = 2 * this.game.volumeMusic;
			this.game.gameMusic.play();
		}
		this.money = 100;
		this.round = 2;
		this.roundtimer = 20000;
		this.walloverlap = false;
		//this.background = this.game.add.image(this.camera.view.x-50 , this.camera.view.y-50, 'background');
		
		this.game.camera.flash(0xffffff, 300);
		this.game.world.setBounds(0, 0, 2100, 980);
		// this.game.camera.follow(this.player);

		this.shoot = true;
		this.pauseBtn = this.game.add.button(this.game.width-25, 25, 'pauseBtn', ()=>{
			this.game.paused = true;
			this.board = this.game.add.image(this.camera.view.x + 600, this.camera.view.y + 525 - 175, 'board');
			this.board.anchor.setTo(0.5, 0.5);
			this.board.scale.setTo(0.8, 0.8);

			this.resume = this.game.add.button(this.camera.view.x + 604, this.camera.view.y + 228, 'resume', ()=>{
				this.resume.destroy();
				this.restart.destroy();
				this.options.destroy();
				this.exit.destroy();
				this.board.destroy();
				this.game.paused = false;
			});
			this.resume.anchor.setTo(0.5, 0.5);
			this.resume.scale.setTo(0.82, 0.82);
			this.resume.onInputOver.add(()=>{
				this.resume.scale.x = 0.84;
				this.resume.scale.y = 0.84;
			}, this);
			this.resume.onInputOut.add(()=>{
				this.resume.scale.x = 0.82;
				this.resume.scale.y = 0.82;
			});

			this.restart = this.game.add.button(this.camera.view.x + 604, this.camera.view.y + 322, 'restart', ()=>{
				this.game.paused = false;
				this.game.state.start('singlePlayer');
			});
			this.restart.anchor.setTo(0.5, 0.5);
			this.restart.scale.setTo(0.82, 0.82);
			this.restart.onInputOver.add(()=>{
				this.restart.scale.x = 0.84;
				this.restart.scale.y = 0.84;
			});
			this.restart.onInputOut.add(()=>{
				this.restart.scale.x = 0.82;
				this.restart.scale.y = 0.82;
			});

			this.options = this.game.add.button(this.camera.view.x + 605, this.camera.view.y + 417, 'options', ()=>{

			});
			this.options.anchor.setTo(0.5, 0.5);
			this.options.scale.setTo(0.82, 0.82);
			this.options.onInputOver.add(()=>{
				this.options.scale.x = 0.84;
				this.options.scale.y = 0.84;
			});
			this.options.onInputOut.add(()=>{
				this.options.scale.x = 0.82;
				this.options.scale.y = 0.82;
			});

			this.exit = this.game.add.button(this.camera.view.x + 604, this.camera.view.y + 517, 'exit', ()=>{
				this.game.paused = false;
				this.game.state.start('menu');
			});
			this.exit.anchor.setTo(0.5, 0.5);
			this.exit.scale.setTo(0.82, 0.82);
			this.exit.onInputOver.add(()=>{
				this.exit.scale.x = 0.84;
				this.exit.scale.y = 0.84;
			});
			this.exit.onInputOut.add(()=>{
				this.exit.scale.x = 0.82;
				this.exit.scale.y = 0.82;
			});

		}, this);
		this.pauseBtn.anchor.setTo(0.5, 0.5);
		this.pauseBtn.scale.setTo(0.1, 0.1);
		this.pauseBtn.onInputOver.add(()=>{
			this.game.add.tween(this.pauseBtn.scale).to({x: 0.11, y: 0.11}, 100).start();
			this.shoot = false;
		}, this);
		this.pauseBtn.onInputOut.add(()=>{
			this.game.add.tween(this.pauseBtn.scale).to({x: 0.1, y: 0.1}, 100).start();
			this.shoot = true;
		}, this);

		this.cursor = this.game.input.keyboard.createCursorKeys();
		

		this.walls = this.game.add.group();
		this.walls.inputEnabled = true;		
		this.walls.enableBody = true;

		this.coins = this.game.add.group();
		this.coins.inputEnabled = true;		
		this.coins.enableBody = true;

		this.buildSound = this.game.add.audio('build');
		this.dieSound = this.game.add.audio('die');
		this.hoverSound = this.game.add.audio('hover');
		this.hurtSound = this.game.add.audio('hurt');
		this.rockHitSound = this.game.add.audio('rockHit');
		this.slingSound = this.game.add.audio('sling');
		this.transmitSound = this.game.add.audio('transmit');
		this.collectMoneySound = this.game.add.audio('collectMoney');
		// this.walls.events.onInputDown.add(()=>{
		// 	var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
		// 	console.log(index);
		// })
		// for(var i=0; i<3; i++){
		// 	for(var j=0; j<60; j++){
		// 		var r = this.game.rnd.between(0,1);
		// 		var str = '';
		// 		if(r < 0.33)
		// 			str = 'stone_2';
		// 		else if(r < 0.66)
		// 			str = 'stone_2_1';
		// 		else
		// 			str = 'stone_2_2';
		// 		var a = this.game.add.sprite(j*28, 644+i*28, str, 0, this.walls);//60px*60px
		// 		a.health = 2;
		// 	}
		// }
		this.map = [];
		if(this.game.mapNumber === 1){
			for(var i=0; i<2625; i++){
				this.map[i] = map1[i];
			}
		}
		else if(this.game.mapNumber === 2){
			for(var i=0; i<2625; i++){
				this.map[i] = map2[i];
			}
		}
		else if(this.game.mapNumber === 3){
			for(var i=0; i<2625; i++){
				this.map[i] = map3[i];
			}
		}
		else if(this.game.mapNumber === 4){
			for(var i=0; i<2625; i++){
				this.map[i] = map4[i];
			}
		}

		for(var i =0; i<35; i++){
			for(var j=0; j<75; j++){
				if(this.map[75*i + j] === 3){
					var r = this.game.rnd.between(0,1);
					var str = '';
					if(r < 0.33)
						str = 'stone_2';
					else if(r < 0.66)
						str = 'stone_2_1';
					else
						str = 'stone_2_2';
					var a = this.game.add.sprite(j*28, i*28, str, 0, this.walls);
					a.health = 3;
					a.inputEnabled = true;
					a.enableBody = true;
					a.events.onInputDown.add((a)=>{
							if(this.player.weapon === 'wall'){
							var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
							if(a.health<3){
								a.health+=1;
								if(a.health==3) a.loadTexture('stone_2');
								else if(a.health==2) a.loadTexture('stone_1');

								this.buildSound.volume = this.game.volumeSound;
								this.buildSound.play();
							}
							this.money-=10;
							this.map[index]+=1;
						}	
					}, this)
				}
				else if(this.map[75*i + j] === 2){
					var r = this.game.rnd.between(0,1);
					var str = '';
					str = 'stone_1';
					var a = this.game.add.sprite(j*28, i*28, str, 0, this.walls);
					a.health = 2;
					a.inputEnabled = true;
					a.enableBody = true;
					a.events.onInputDown.add((a)=>{
							if(this.player.weapon === 'wall'){
							var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
							if(a.health<3){
								a.health+=1;
								if(a.health==3) a.loadTexture('stone_2');
								else if(a.health==2) a.loadTexture('stone_1');

								this.buildSound.volume = this.game.volumeSound;
								this.buildSound.play();
							}
							this.money-=10;
							this.map[index]+=1;
						}	
					}, this)
				}
				else if(this.map[75*i + j] === 1){
					var r = this.game.rnd.between(0,1);
					var str = '';
					str = 'stone';
					var a = this.game.add.sprite(j*28, i*28, str, 0, this.walls);
					a.health = 1;
					a.inputEnabled = true;
					a.enableBody = true;
					a.events.onInputDown.add((a)=>{
							if(this.player.weapon === 'wall'){
							var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
							if(a.health<3){
								a.health+=1;
								if(a.health==3) a.loadTexture('stone_2');
								else if(a.health==2) a.loadTexture('stone_1');

								this.buildSound.volume = this.game.volumeSound;
								this.buildSound.play();
							}
							this.money-=10;
							this.map[index]+=1;
						}	
					}, this)
				}
				else{
					
				}
			}
		}

		// for(var i=0;i<100;i++){
		// 	this.game.add.sprite(28*this.game.rnd.between(0,50), 28*this.game.rnd.between(10,22), 'stone', 0, this.walls);
		// }
		
		this.arrow = this.game.add.sprite(this.game.width/2, this.game.height, 'arrow');
		this.game.physics.arcade.enable(this.arrow);
		this.arrow.anchor.setTo(0,0.5);
		this.arrow.visible = false;

		this.wallcursor = this.game.add.sprite((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28, (this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28, 'stone');
		this.game.physics.arcade.enable(this.wallcursor);
		this.wallcursor.visible = false;
		this.wallcursor.alpha = 0.5;

		this.transmissionCursor = this.game.add.sprite((this.game.input.x+this.camera.view.x), (this.game.input.y+this.camera.view.y), 'transmissionCursor');
		this.transmissionCursor.anchor.setTo(0.5, 0.5);
		this.game.physics.arcade.enable(this.transmissionCursor);
		this.transmissionCursor.visible = false;
		this.transmissionCursor.alpha = 0.5;

		this.players_1 = this.game.add.group();
		this.players_2 = this.game.add.group();
		this.player_healths = this.game.add.group();
		this.player_healths_in = this.game.add.group();
		this.rects = this.game.add.group();
		for(var i=0; i<3; i++){
			this.create_player_1(i);
			this.create_player_2(i);
		}
		

		this.weapon = this.game.add.sprite(this.player.x,this.player.y, 'gun');
		this.weapon.anchor.setTo(0,0.5);

		this.bullets = this.game.add.sprite(this.player.x,this.player.y, 'rockBullet');
		this.bullets.kill();

		this.pointer = this.game.add.sprite(this.player.x,this.player.y-80,'pointer');
		this.pointer.anchor.setTo(0.5,0.5);
		
		
		
		this.txt1 = this.game.add.text(100+this.camera.view.x, 50+this.camera.view.y, this.money, {
			font: '24px font',
			fill: '#ffffff'
		});
		this.txt1.anchor.setTo(0.5,0.5);
		this.txt2 = this.game.add.text(600+this.camera.view.x, 100+this.camera.view.y, this.roundtimer, {
			font: '24px font',
			fill: '#ffffff'
		});
		this.txt2.anchor.setTo(0.5,0.5);
		this.txt3 = this.game.add.text(600+this.camera.view.x, 350+this.camera.view.y, "", {
			font: '60px font',
			fill: '#000000'
		});
		this.txt3.anchor.setTo(0.5, 0.5);
		this.txt3.visible = false;

		this.moneyIcon = this.game.add.image(this.camera.view.x+50, 50+this.camera.view.y,'moneyIcon');
		this.timerIcon = this.game.add.image(600+this.camera.view.x, 50+this.camera.view.y,'timerIcon');
		this.moneyIcon.anchor.setTo(0.5,0.5);
		this.timerIcon.anchor.setTo(0.5,0.5);


		//this.game.add.sprite(336, 622, 'stone', 0, this.walls);
		//this.game.add.sprite(308, 594, 'stone', 0, this.walls);
		//this.game.add.sprite(280, 566, 'stone', 0, this.walls);
		//this.game.add.sprite(256, 538, 'stone', 0, this.walls);
		//
		//this.game.add.sprite(290, 444, 'stone', 0, this.walls);
		//this.game.add.sprite(320, 416, 'stone', 0, this.walls);
		//this.game.add.sprite(365, 388, 'stone', 0, this.walls);
		//
		//this.game.add.sprite(456, 322, 'stone', 0, this.walls);
		//this.game.add.sprite(512, 266, 'stone', 0, this.walls);
		//this.game.add.sprite(540, 238, 'stone', 0, this.walls);

		//this.game.add.sprite(670, 238, 'stone', 0, this.walls);
		//this.game.add.sprite(710, 270, 'stone', 0, this.walls);
		
		//this.game.add.sprite(200, 500, 'stone');
		//this.test= this.game.add.sprite(1000, 500, 'stone');
		//this.game.physics.arcade.enable(this.test);
		//this.test.enableBody = true;
		//this.test.body.immovable = true;
		//this.game.physics.arcade.collide(this.players_1, this.test);
		//this.game.add.sprite(400, 350, 'stone' );
		//this.game.add.sprite(800, 350, 'stone' );
		//this.game.add.sprite(600, 200, 'stone' );

		//this.game.add.sprite(228, 528, 'stone', 0, this.walls);
		//this.game.add.sprite(1028, 528, 'stone', 0, this.walls);
		//this.game.add.sprite(428, 378, 'stone', 0, this.walls);
		//this.game.add.sprite(828, 378, 'stone', 0, this.walls);
		//this.game.add.sprite(628, 228, 'stone', 0, this.walls);

		//this.game.add.sprite(228, 500, 'stone', 0, this.walls);
		//this.game.add.sprite(1028, 500, 'stone', 0, this.walls);
		//this.game.add.sprite(428, 350, 'stone', 0, this.walls);
		//this.game.add.sprite(828, 350, 'stone', 0, this.walls);
		//this.game.add.sprite(628, 200, 'stone', 0, this.walls);

		//this.game.add.sprite(200, 528, 'stone', 0, this.walls);
		//this.game.add.sprite(1000, 528, 'stone', 0, this.walls);
		//this.game.add.sprite(400, 378, 'stone', 0, this.walls);
		//this.game.add.sprite(800, 378, 'stone', 0, this.walls);
		//this.game.add.sprite(600, 228, 'stone', 0, this.walls);
		//this.bottomfloor = this.game.add.sprite(0, this.game.height-33, 'ground', 0, this.walls); 
		//this.bottomfloor.scale.setTo(7,1);
		this.walls.setAll('body.immovable', true);


		this.explosions = this.game.add.group();
        this.explosions.createMultiple(100, 'boom');
		this.explosions.forEach(this.setupBoomer, this);
		this.game.physics.arcade.enable(this.explosions);

		// this.camera.scale.x = 1.5;
		// this.camera.scale.y = 1.5;
		// zoom in/out
		// this.game.input.mouse.mouseWheelCallback = ()=>{
		// 	this.camera.focusOnXY(this.game.input.x, this.game.input.y);
		// 	if(this.input.mouse.wheelDelta === Phaser.Mouse.WHEEL_UP) {
		// 		this.camera.scale.x += 0.01;
		// 		this.camera.scale.y += 0.01;
		// 	}
		// 	else{
		// 		this.camera.scale.x -= 0.01;
		// 		this.camera.scale.y -= 0.01;
		// 	}
		// };

		this.drag_right = false;
		this.drag_left = false;
		this.drag_up = false;
		this.drag_down = false;

		this.drag_u = this.game.add.button(this.game.width / 2, 25, 'drag_u', ()=>{
			
		}, this);
		this.drag_u.anchor.setTo(0.5, 0.5);
		this.drag_u.scale.setTo(0.05, 0.05);
		this.drag_u.onInputOver.add(()=>{
			this.game.add.tween(this.drag_u.scale).to({x: 0.07, y: 0.07}, 100).start();
			this.drag_up = true;
		}, this);
		this.drag_u.onInputOut.add(()=>{
			this.game.add.tween(this.drag_u.scale).to({x: 0.05, y: 0.05}, 100).start();
			this.drag_up = false;
		}, this);

		this.drag_d = this.game.add.button(this.game.width / 2, 675, 'drag_d', ()=>{
			
		}, this);
		this.drag_d.anchor.setTo(0.5, 0.5);
		this.drag_d.scale.setTo(0.05, 0.05);
		this.drag_d.onInputOver.add(()=>{
			this.game.add.tween(this.drag_d.scale).to({x: 0.07, y: 0.07}, 100).start();
			this.drag_down = true;
		}, this);
		this.drag_d.onInputOut.add(()=>{
			this.game.add.tween(this.drag_d.scale).to({x: 0.05, y: 0.05}, 100).start();
			this.drag_down = false;
		}, this);

		this.drag_r = this.game.add.button(1175, this.game.height / 2, 'drag_r', ()=>{
			
		}, this);
		this.drag_r.anchor.setTo(0.5, 0.5);
		this.drag_r.scale.setTo(0.05, 0.05);
		this.drag_r.onInputOver.add(()=>{
			this.game.add.tween(this.drag_r.scale).to({x: 0.07, y: 0.07}, 100).start();
			this.drag_right = true;
		}, this);
		this.drag_r.onInputOut.add(()=>{
			this.game.add.tween(this.drag_r.scale).to({x: 0.05, y: 0.05}, 100).start();
			this.drag_right = false;
		}, this);

		this.drag_l = this.game.add.button(25, this.game.height / 2, 'drag_l', ()=>{
			
		}, this);
		this.drag_l.anchor.setTo(0.5, 0.5);
		this.drag_l.scale.setTo(0.05, 0.05);
		this.drag_l.onInputOver.add(()=>{
			this.game.add.tween(this.drag_l.scale).to({x: 0.07, y: 0.07}, 100).start();
			this.drag_left = true;
		}, this);
		this.drag_l.onInputOut.add(()=>{
			this.game.add.tween(this.drag_l.scale).to({x: 0.05, y: 0.05}, 100).start();
			this.drag_left = false;
		}, this);


		this.emitter = this.game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 500);
		this.emitter.gravity = 500;
		this.chooseWeaponBar = null;
		this.playerHover = false;

		
	},
	
	update(){
		this.txt1.x = 100+this.camera.view.x;
		this.txt1.y = 50+this.camera.view.y;
		this.txt1.text = this.money;
		this.txt2.x = 600+this.camera.view.x;
		this.txt2.y = 100+this.camera.view.y;
		var timeleft = this.roundtimer-this.game.time.now
		this.txt2.text = (timeleft-timeleft%1000)/1000;

		this.moneyIcon.x = this.camera.view.x+50;
		this.moneyIcon.y = 50+this.camera.view.y;
		this.timerIcon.x = 600+this.camera.view.x
		this.timerIcon.y = 50+this.camera.view.y;

		
		this.game.physics.arcade.collide(this.players_1, this.walls);
		this.game.physics.arcade.collide(this.players_2, this.walls);
		this.game.physics.arcade.collide(this.coins, this.walls, this.coin_collide_wall, null, this);
		this.game.physics.arcade.overlap(this.bullets, this.players_1, this.bullet_collide_player, null, this);
		this.game.physics.arcade.overlap(this.bullets, this.players_2, this.bullet_collide_player, null, this);
		this.game.physics.arcade.collide(this.bullets, this.walls,this.bullet_collide_wall, null, this);

		this.game.physics.arcade.overlap(this.coins, this.players_1, this.coin_collide_player1, null, this);
		this.game.physics.arcade.overlap(this.coins, this.players_2, this.coin_collide_player2, null, this);

		this.game.physics.arcade.overlap(this.explosions, this.walls, this.explosion_wall, null, this);
		this.game.physics.arcade.overlap(this.explosions, this.players_1, this.explosion_player, null, this);
		this.game.physics.arcade.overlap(this.explosions, this.players_2, this.explosion_player, null, this);

			  
		this.background.x = this.camera.view.x-50;
		this.background.y = this.camera.view.y-50;

      	this.weapon.x = this.player.x;
		this.weapon.y = this.player.y;

		this.pointer.x = this.player.x;
		this.pointer.y = this.player.y-80;
		for(var i = 0;i<6;i++){
			
			if(i<3){
		        if(!this.players_1.getChildAt(i).alive){
					this.player_healths.getChildAt(i).kill();
					this.player_healths_in.getChildAt(i).kill();
				}
				else{
					this.player_healths.getChildAt(i).x = this.players_1.getChildAt(i).x;
					this.player_healths.getChildAt(i).y = this.players_1.getChildAt(i).y-50;
					
					this.player_healths_in.getChildAt(i).x = this.players_1.getChildAt(i).x-24;
					this.player_healths_in.getChildAt(i).y = this.players_1.getChildAt(i).y-50;
	
					this.player_healths_in.getChildAt(i).scale.x = this.players_1.getChildAt(i).health/100;
				}
				

			}
			else{
				if(!this.players_2.getChildAt(i%3).alive){
					this.player_healths.getChildAt(i).kill();
					this.player_healths_in.getChildAt(i).kill();
				}
				else{
					this.player_healths.getChildAt(i).x = this.players_2.getChildAt(i%3).x;
				this.player_healths.getChildAt(i).y = this.players_2.getChildAt(i%3).y-50;

				this.player_healths_in.getChildAt(i).x = this.players_2.getChildAt(i%3).x-24;
				this.player_healths_in.getChildAt(i).y = this.players_2.getChildAt(i%3).y-50;

				this.player_healths_in.getChildAt(i).scale.x = this.players_2.getChildAt(i%3).health/100;
				}
				
			}
		}

		if(this.players_1.getFirstAlive()===null){
			this.txt3.x = 600+this.camera.view.x;
			this.txt3.y = 300+this.camera.view.y;
			this.txt3.text = "PLAYER 2 WIN!!";
			this.txt3.visible = true;
			var aaa = this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
					this.game.paused = false;
					this.game.state.start('singlePlayer');
				}, this);
			aaa.anchor.setTo(0.5, 0.5);
			var bbb = this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
					this.game.paused = false;
					this.game.state.start('menu');
				}, this);
			bbb.anchor.setTo(0.5, 0.5);
				this.game.paused = true;
			}
			else if(this.players_2.getFirstAlive()===null){
				this.txt3.x = 600+this.camera.view.x;
				this.txt3.y = 300+this.camera.view.y;
				this.txt3.text = "PLAYER 1 WIN!!";
				this.txt3.visible = true;
				this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
						this.game.paused = false;
						this.game.state.start('singlePlayer');
					}, this).anchor.setTo(0.5, 0.5);
					this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
							this.game.paused = false;
							this.game.state.start('menu');
						}, this);
						this.game.paused = true;
					}


		if (!this.player.alive) { 
			if(this.round==1)this.player = this.players_1.getFirstAlive();
			else if(this.round==2)this.player = this.players_2.getFirstAlive();
		}
     		  

      	if(this.bullets.alive)this.roundtimer = this.game.time.now;
      	else if(this.roundtimer<this.game.time.now&&!this.bullets.alive){
      		if(this.round==1){
			    
				if(this.players_1.getFirstAlive()===null){
					this.txt3.x = 600+this.camera.view.x;
					this.txt3.y = 300+this.camera.view.y;
					this.txt3.text = "PLAYER 2 WIN!!";
					this.txt3.visible = true;
					var aaa = this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
							this.game.paused = false;
							this.game.state.start('singlePlayer');
						}, this);
					aaa.anchor.setTo(0.5, 0.5);
					var bbb = this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
							this.game.paused = false;
							this.game.state.start('menu');
						}, this);
					bbb.anchor.setTo(0.5, 0.5);
						this.game.paused = true;
					}
					else if(this.players_2.getFirstAlive()===null){
						this.txt3.x = 600+this.camera.view.x;
						this.txt3.y = 300+this.camera.view.y;
						this.txt3.text = "PLAYER 1 WIN!!";
						this.txt3.visible = true;
						this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
								this.game.paused = false;
								this.game.state.start('singlePlayer');
							}, this).anchor.setTo(0.5, 0.5);
							this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
									this.game.paused = false;
									this.game.state.start('menu');
								}, this).anchor.setTo(0.5, 0.5);
								this.game.paused = true;
							}
				else {
					this.player.body.velocity.x = 0;
					this.weapon.loadTexture('gun');
					this.player = this.players_2.getFirstAlive();
					this.player.weapon = 'gun';
				    this.round = 2;
				}
				this.roundtimer+=20000;
      		}
      		else if(this.round==2){
				
				if(this.players_1.getFirstAlive()===null){
					this.txt3.x = 600+this.camera.view.x;
					this.txt3.y = 300+this.camera.view.y;
					this.txt3.text = "PLAYER 2 WIN!!";
					this.txt3.visible = true;
					var aaa = this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
							this.game.paused = false;
							this.game.state.start('singlePlayer');
						}, this);
					aaa.anchor.setTo(0.5, 0.5);
					var bbb = this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
							this.game.paused = false;
							this.game.state.start('menu');
						}, this);
					bbb.anchor.setTo(0.5, 0.5);
						this.game.paused = true;
					}
					else if(this.players_2.getFirstAlive()===null){
						this.txt3.x = 600+this.camera.view.x;
						this.txt3.y = 300+this.camera.view.y;
						this.txt3.text = "PLAYER 1 WIN!!";
						this.txt3.visible = true;
						this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
								this.game.paused = false;
								this.game.state.start('singlePlayer');
							}, this).anchor.setTo(0.5, 0.5);
							this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
									this.game.paused = false;
									this.game.state.start('menu');
								}, this).anchor.setTo(0.5, 0.5);
								this.game.paused = true;
							}
				else {
					this.player.body.velocity.x = 0;
					this.weapon.loadTexture('gun');
					this.player = this.players_1.getFirstAlive();
					this.player.weapon = 'gun';
					this.computer_shot();
					// console.log(this.player.health);
				    this.round = 1;
				}
      		}
      		
		}
		
      	
      	if(this.player !== null){
			if(this.game.input.x + this.camera.view.x<this.player.x) this.weapon.scale.y=-1;
			else this.weapon.scale.y = 1;
		} 
      	      
      	if(this.chooseWeaponBar !== null){
			this.chooseWeaponBar.x = this.camera.view.x + 600;
			this.chooseWeaponBar.y = this.camera.view.y + 600;
			this.gunIcon.x = this.camera.view.x + 190;
			this.gunIcon.y = this.camera.view.y + 557;
			this.boomIcon.x = this.camera.view.x + 290;
			this.boomIcon.y = this.camera.view.y + 557;
			this.transmissionIcon.x = this.camera.view.x + 450;
			this.transmissionIcon.y = this.camera.view.y + 525;
			this.buildwallIcon.x = this.camera.view.x + 400;
			this.buildwallIcon.y = this.camera.view.y + 557;
			this.balaIcon.x = this.camera.view.x + 600;
			this.balaIcon.y = this.camera.view.y + 557+5;
			this.canonIcon.x = this.camera.view.x + 700;
			this.canonIcon.y = this.camera.view.y + 557+5;
			this.meatIcon.x = this.camera.view.x + 800;
			this.meatIcon.y = this.camera.view.y + 557+5;
			for(var i=0; i<2; i++){
				for(var j=0; j<9; j++){
					this.myBlock[i*9 + j].x = this.camera.view.x + 150 + j*100;
					this.myBlock[i*9 + j].y = this.camera.view.y + 525 + i*75;
				}
			}      
      	}
      	if(this.player !== null) this.weapon.rotation = this.game.physics.arcade.angleBetween(this.player, {
      		x: this.game.input.x/this.camera.scale.x+(this.camera.view.x)/this.camera.scale.x,
      		y: this.game.input.y/this.camera.scale.y+(this.camera.view.y)/this.camera.scale.y
      	});      //console.log(!this.player.input.pointerOver() );
      	if (this.game.input.mousePointer.isDown && !this.playerHover &&this.shoot && this.bullets.alive === false && this.chooseWeaponBar === null && this.player.weapon !== 'wall')
      	{
      		this.arrow.visible = true;
      		this.arrow.body.x = this.player.body.x + 15;
      		this.arrow.body.y = this.player.body.y + 8;
      		this.arrow.rotation = this.game.physics.arcade.angleBetween(this.player, {
      			x: this.game.input.x/this.camera.scale.x+(this.camera.view.x)/this.camera.scale.x,
      			y: this.game.input.y/this.camera.scale.y+(this.camera.view.y)/this.camera.scale.y
      		});
      		this.player.power += 0.015 * this.player.power_;
      		if(this.player.power <= 0.2) this.player.power_ = 1;
      		else if(this.player.power >= 1) this.player.power_ = -1;
      		this.arrow.scale.setTo(this.player.power, 1);
      		
      	}		
      	else if(this.player !== null && this.shoot && this.bullets.alive === false && !this.playerHover  && this.chooseWeaponBar === null&&this.player.weapon!='wall'){
      		if(this.arrow.visible){
      			this.arrow.visible = false;
				  this.addbullet();
				  
				  if(this.player.weapon === 'boom'){
					this.slingSound.volume = 2*this.game.volumeSound;
					this.slingSound.play();
				  }
      		}
      		this.player.power = 0.2;
      	}
      	if (this.player !== null && this.player.weapon === 'wall'){
      		//console.log(this.walloverlap);
      		this.wallcursor.visible = true;
      		this.wallcursor.x = (this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28;
      		this.wallcursor.y = (this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28;
			this.game.input.onDown.add(this.addwall, this);
      	}
      	else {
      		this.wallcursor.visible = false;
		}
		if(this.player !== null && this.player.weapon === 'transmission'){
			this.transmissionCursor.visible = true;
      		this.transmissionCursor.x = (this.game.input.x+this.camera.view.x);
      		this.transmissionCursor.y = (this.game.input.y+this.camera.view.y);
			this.game.input.onDown.add(this.transmit, this);
		}
		else {
			this.transmissionCursor.visible = false;
		}

      	if(this.player !== null && this.player.body.touching.down) this.land();      
		if(this.round == 2) this.movePlayer();
		this.play_animations();    


		if(this.game.input.x >= 1000 && this.game.input.y > 200 && this.game.input.y < 500){
			this.drag_r.alpha = 1;
		}
		else {
			this.drag_r.alpha = 0;
		}
		if(this.game.input.x <= 200 && this.game.input.y > 200 && this.game.input.y < 500){
			this.drag_l.alpha = 1;
		}
		else {
			this.drag_l.alpha = 0;
		}
		
		if(this.game.input.y <= 200 && this.game.input.x > 400 && this.game.input.x < 800){
			this.drag_u.alpha = 1;
		}
		else {
			this.drag_u.alpha = 0;
		}

		if(this.game.input.y >= 500 && this.game.input.x > 400 && this.game.input.x < 800){
			this.drag_d.alpha = 1;
		}
		else {
			this.drag_d.alpha = 0;
		}

		if(this.drag_up){
			this.camera.y -= 8;
		}
		else if(this.drag_down){
			this.camera.y += 8;
		}
		else if(this.drag_right){
			this.camera.x += 8;
		}
		else if(this.drag_left){
			this.camera.x -= 8;
		}
		

		this.pauseBtn.x = this.camera.view.x + 1175;
		this.pauseBtn.y = this.camera.view.y + 25;
		this.drag_d.x = this.camera.view.x + 600;
		this.drag_d.y = this.camera.view.y + 675;
		this.drag_u.x = this.camera.view.x + 600;
		this.drag_u.y = this.camera.view.y + 25;
		this.drag_r.x = this.camera.view.x + 1175;
		this.drag_r.y = this.camera.view.y + 350;
		this.drag_l.x = this.camera.view.x + 25;
		this.drag_l.y = this.camera.view.y + 350;
	},
	computer_shot(){


		//  enemyBullet = enemyBullets.getFirstExists(false);
	  enemyBullet = true;
	  var livingPlayer_1 = [];
	  var livingPlayer_2 = [];
	  livingPlayer_1.length=0;
	  livingPlayer_2.length=0;
  
	  this.players_1.forEachAlive(function(enemy){
  
		  // put every living enemy in an array
		  livingPlayer_1.push(enemy);
	  });
	  this.players_2.forEachAlive(function(enemy){
  
		  // put every living enemy in an array
		  livingPlayer_2.push(enemy);
	  });
  
	  if (enemyBullet && livingPlayer_1.length > 0 && livingPlayer_2.length > 0)
	  {
		  
		  var random1=this.game.rnd.integerInRange(0,livingPlayer_1.length-1);
		  var random2=this.game.rnd.integerInRange(0,livingPlayer_2.length-1);
  
		  // randomly select one of them
		  var shooter=livingPlayer_1[random1];
		  this.player = livingPlayer_1[random1]
		  var target=livingPlayer_2[random2];
		  // And fire the bullet from this enemy
		  this.bullets.reset(shooter.x, shooter.y);
		  this.bullets.enableBody = true;
		  this.game.physics.arcade.enable(this.bullets);
		  this.bullets.anchor.setTo(0.5,0.5);
		  this.bullets.body.gravity.y = 1000; 
		  this.bullets.checkWorldBounds = true;
		  this.bullets.outOfBoundsKill = true;
		  this.bullets.damage = 50;
		  this.bullets.loadTexture('rockBullet');
		  this.bullets.revive();
		  
		  if(target.x > shooter.x && (shooter.y - target.y) < (target.x - shooter.x)){
			  
				var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs(target.x-shooter.x+target.y-shooter.y)/this.bullets.body.gravity.y);
				velocity = Math.min(560, velocity);
				this.bullets.body.velocity.x = velocity;
				this.bullets.body.velocity.y = -velocity;
				console.log("state=1 "+velocity);
		  }
		  else if(target.x < shooter.x && (shooter.y - target.y) < -(target.x - shooter.x)){
			var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs((-1)*(target.x-shooter.x)+target.y-shooter.y)/this.bullets.body.gravity.y);
			velocity = Math.max(-560, velocity);
			//console.log(velocity)
			this.bullets.body.velocity.x = velocity;
			this.bullets.body.velocity.y = velocity;
			//console.log("state=2 "+velocity);
		  }
		  else if(target.x > shooter.x && (shooter.y - target.y) >= (target.x - shooter.x) && (shooter.y - target.y) <= 4*(target.x - shooter.x)){
			var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs(4*(target.x-shooter.x)+target.y-shooter.y)/this.bullets.body.gravity.y);
			velocity = Math.min(192+30, velocity);
			//console.log(velocity)
			this.bullets.body.velocity.x = velocity;
			this.bullets.body.velocity.y = -4*velocity;
			//console.log("state=3 "+velocity);
		  }
		  else if(target.x < shooter.x && (shooter.y - target.y) >= -(target.x - shooter.x) && (shooter.y - target.y) <= (-4)*(target.x - shooter.x)){
			var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs((-4)*(target.x-shooter.x)+target.y-shooter.y)/this.bullets.body.gravity.y);
			velocity = Math.max(-192-30, velocity);
			//console.log(velocity)
			this.bullets.body.velocity.x = velocity;
			this.bullets.body.velocity.y = 4*velocity;
			//console.log("state=4 "+velocity);
		  }
		  else if(target.x > shooter.x){
			var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs(15*(target.x-shooter.x)+target.y-shooter.y)/this.bullets.body.gravity.y);
			velocity = Math.min(53+30, velocity);
			//console.log(velocity)
			this.bullets.body.velocity.x = velocity;
			this.bullets.body.velocity.y = -15*velocity;
			//console.log("state=5 "+velocity);
		  }
		  else{
			var velocity = (target.x-shooter.x)/Math.sqrt(2*Math.abs((-15)*(target.x-shooter.x)+target.y-shooter.y)/this.bullets.body.gravity.y);
			velocity = Math.max(-53-30, velocity);
			//console.log(velocity)
			this.bullets.body.velocity.x = velocity;
			this.bullets.body.velocity.y = 15*velocity;
			//console.log("state=6 "+velocity);
		  }
		  //console.log("x="+(target.x-shooter.x)+"   y="+(target.y-shooter.y)+"    v="+velocity);
		  
		  //this.game.physics.arcade.moveToObject(this.bullets,target,120);
		  //firingTimer = game.time.now + 2000;
	  }

	},
	
	create_player_1(k){
		if(this.game.mapNumber === 1){
			var newplayer = this.game.add.sprite(154+k*168+this.game.rnd.between(-63,63), this.game.height/2, 'player', 0 , this.players_1);
		}
		else if(this.game.mapNumber === 2){
			var newplayer = this.game.add.sprite(448+k*168+this.game.rnd.between(-250,250), 280, 'player', 0 , this.players_1);
		}
		else if(this.game.mapNumber === 3){
			var newplayer = this.game.add.sprite(550+this.game.rnd.between(-100,100), 150+k*242+this.game.rnd.between(-91,91), 'player', 0 , this.players_1);
		}
		else if(this.game.mapNumber === 4){
			var newplayer = this.game.add.sprite(284+k*94+this.game.rnd.between(-35,35), 250+k*225+this.game.rnd.between(-84,84), 'player', 0 , this.players_1);
		}
		//var newplayer = this.game.add.sprite(this.game.width/2+this.game.rnd.between(-500,-100), this.game.height-100+this.game.rnd.between(-250,-200), 'player', 0 , this.players_1);
		this.game.physics.arcade.enable(newplayer);
		newplayer.inputEnabled = true;
		newplayer.body.gravity.y = 1000;
		newplayer.anchor.setTo(0.5,0.5);
		newplayer.jumping = false;
		newplayer.power = 1;
		newplayer.power_ = -1;
		newplayer.weapon = 'gun';
		newplayer.health = 100;
		newplayer.animations.add('idle',[7],8,true);
		newplayer.animations.play('idle');
		newplayer.checkWorldBounds = true;
		newplayer.outOfBoundsKill = true;
		this.player = newplayer;

		var newhealth_ = this.game.add.sprite(newplayer.x-24,newplayer.y-50,'health_in',0,this.player_healths_in);
		newhealth_.anchor.setTo(0,0.5);
		this.game.physics.arcade.enable(newhealth_);


		var newhealth = this.game.add.sprite(newplayer.x,newplayer.y-50,'health_out',0,this.player_healths);
		newhealth.anchor.setTo(0.5,0.5);
		this.game.physics.arcade.enable(newhealth);
		
	},
	create_player_2(r){
		if(this.game.mapNumber === 1){
			var newplayer = this.game.add.sprite(1946-r*168+this.game.rnd.between(-63,63), this.game.height/2, 'player_1', 0 , this.players_2);
		}
		else if(this.game.mapNumber === 2){
			var newplayer = this.game.add.sprite(1652-r*168+this.game.rnd.between(-250,250), 280, 'player_1', 0 , this.players_2);
		}
		else if(this.game.mapNumber === 3){
			var newplayer = this.game.add.sprite(1550+this.game.rnd.between(-100,100), 150+r*242+this.game.rnd.between(-91,91), 'player_1', 0 , this.players_2);
		}
		else if(this.game.mapNumber === 4){
			var newplayer = this.game.add.sprite(1816-r*94+this.game.rnd.between(-35,35), 250+r*225+this.game.rnd.between(-84,84), 'player_1', 0 , this.players_2);
		}
		//var newplayer = this.game.add.sprite(this.game.width/2+this.game.rnd.between(300,600), this.game.height-100+this.game.rnd.between(-200,-100), 'player_1', 0 , this.players_2);
		this.game.physics.arcade.enable(newplayer);
		newplayer.inputEnabled = true;
		newplayer.body.gravity.y = 1000;
		newplayer.anchor.setTo(0.5,0.5);
		newplayer.jumping = false;
		newplayer.checkWorldBounds = true;
		newplayer.outOfBoundsKill = true;
		newplayer.power = 1;
		newplayer.power_ = -1;
		newplayer.health = 100;
		newplayer.weapon = 'gun';

		newplayer.animations.add('walk',[0,1,2,3,4,5,6], 8, true);
		newplayer.animations.add('idle',[7],8,true);
		newplayer.animations.add('jump',[8,9,10,11], 8, true);

		newplayer.gun_bought = true;
		newplayer.boom_bought = false;

		var newhealth_ = this.game.add.sprite(newplayer.x-24,newplayer.y-50,'health_in',0,this.player_healths_in);
		newhealth_.anchor.setTo(0,0.5);
		this.game.physics.arcade.enable(newhealth_);

		var newhealth = this.game.add.sprite(newplayer.x,newplayer.y-50,'health_out',0,this.player_healths);
		newhealth.anchor.setTo(0.5,0.5);
		this.game.physics.arcade.enable(newhealth);

		
		newplayer.events.onInputOver.add(()=>{
			this.playerHover = true;
		}, this);
		newplayer.events.onInputOut.add(()=>{
			this.playerHover = false;
		}, this)
		newplayer.events.onInputDown.add(()=>{
			if(this.player === newplayer && this.chooseWeaponBar === null){
				this.chooseWeaponBar = this.game.add.image(this.camera.view.x + 600, this.camera.view.y + 600, 'chooseWeaponBar');

				this.myBlock = [];
				for(var i=0; i<2; i++){
					for(var j=0; j<9; j++){
						this.myBlock.push(
							this.game.add.button(this.camera.view.x + j*100 + 150, this.camera.view.y + i*75 + 525, 'transparentBg', ((i, j)=>{
								return ()=>{
								switch(i*9 + j){
									case 0: {
										newplayer.weapon = 'gun';
										this.weapon.loadTexture('gun');
										break;
									}
									case 1: {
										if(newplayer.boom_bought==true) newplayer.weapon = 'boom';
										else{
											this.money-=50;
											newplayer.boom_bought = true;
											newplayer.weapon = 'boom';
										}
										this.weapon.loadTexture('sling');
										break;
									}
									case 2: {
										newplayer.weapon = 'wall';
										this.weapon.loadTexture('stone');
										break;
									}
									case 3: {
										newplayer.weapon = 'transmission';
										this.weapon.loadTexture('transmissionWeapon');
										break;
									}
									case 4: {
										newplayer.weapon = 'bala';
										this.weapon.loadTexture('bala');
										break;
									}
									case 5: {
										newplayer.weapon = 'canon';
										this.weapon.loadTexture('canon');
										break;
									}
									case 6: {
										this.player.health = 100;
										console.log("!!!!!!!!!!!!!!!!!");
										break;
									}
								}
								this.game.add.tween(this.chooseWeaponBar).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.gunIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.buildwallIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.boomIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.transmissionIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.balaIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.canonIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								this.game.add.tween(this.meatIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);


								this.myBlock.forEach((el)=>{
									this.game.add.tween(el).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
								});
								
								this.game.time.events.add(200, ()=>{
									this.destroyweaponbar();
								}, this);
								}
							})(i, j), this)
						);
					}
				}

				this.myBlock.forEach((el)=>{
					el.events.onInputOver.add(()=>{
						el.loadTexture('transparentBg_1');
						this.hoverSound.volume = this.game.volumeSound;
						this.hoverSound.play();
					});
					el.events.onInputOut.add(()=>{
						el.loadTexture('transparentBg');
					})
				});

				this.gunIcon = this.game.add.image(this.camera.view.x + 125, this.camera.view.y + 557, 'gun');
				this.gunIcon.anchor.setTo(0.3, 0.5);

				this.boomIcon = this.game.add.image(this.camera.view.x + 225, this.camera.view.y + 557, 'slingIcon');
				this.boomIcon.anchor.setTo(0.4, 0.4);

				this.buildwallIcon = this.game.add.image(this.camera.view.x + 250, this.camera.view.y + 557, 'buildIcon');
				this.buildwallIcon.anchor.setTo(0.5, 0.5);

				this.transmissionIcon = this.game.add.image(this.camera.view.x + 250, this.camera.view.y + 557, 'transmissionIcon');
				// this.transmissionIcon.anchor.setTo(0.5, 0.5);

				this.balaIcon = this.game.add.image(this.camera.view.x + 500, this.camera.view.y + 557+5, 'balaIcon');
				this.balaIcon.anchor.setTo(0.5, 0.5);

				this.canonIcon = this.game.add.image(this.camera.view.x + 600, this.camera.view.y + 557+5, 'canonIcon');
				this.canonIcon.anchor.setTo(0.5, 0.5);
			
				this.meatIcon = this.game.add.image(this.camera.view.x + 600, this.camera.view.y + 557+5, 'meatIcon');
				this.meatIcon.anchor.setTo(0.5, 0.5);

				this.chooseWeaponBar.anchor.setTo(0.5, 0.5);
				this.chooseWeaponBar.alpha = 0;

				this.game.add.tween(this.chooseWeaponBar).to( { alpha: 0.6 }, 200, Phaser.Easing.Linear.None, true);
			}else if(this.player === newplayer){
				this.game.add.tween(this.chooseWeaponBar).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.gunIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.buildwallIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.boomIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.transmissionIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.balaIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.canonIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.game.add.tween(this.meatIcon).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				this.myBlock.forEach((el)=>{
					this.game.add.tween(el).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true);
				});
				
				this.game.time.events.add(200, ()=>{
					this.destroyweaponbar();
				}, this);
			}
			
			if(this.player!==newplayer){
				this.player = newplayer;
				this.player.weapon = 'gun';
				this.weapon.loadTexture('gun');
			}

		}, this);
		this.player = newplayer;
	},

	destroyweaponbar(){
		this.chooseWeaponBar.destroy();
		this.boomIcon.destroy();
		this.gunIcon.destroy();
		this.buildwallIcon.destroy();
		this.transmissionIcon.destroy();
		this.balaIcon.destroy();
		this.canonIcon.destroy();
		this.meatIcon.destroy();
		this.myBlock.forEach((el)=>{
			el.destroy();
		});
		this.chooseWeaponBar = null;
	},

	addbullet(){
		if(this.player.weapon=='gun'){
			this.bullets.x = this.player.x;
			this.bullets.y = this.player.y;
			this.bullets.enableBody = true;
			this.game.physics.arcade.enable(this.bullets);
			this.bullets.anchor.setTo(0.5,0.5);
			this.bullets.body.gravity.y = 600; 
			this.bullets.checkWorldBounds = true;
			this.bullets.outOfBoundsKill = true;
			this.bullets.damage = 50;
			this.bullets.loadTexture('rockBullet');
			this.bullets.revive();
		
			this.game.physics.arcade.moveToPointer(this.bullets, 800 * this.player.power);
		}
		if(this.player.weapon=='boom'){
			this.bullets.x = this.player.x;
			this.bullets.y = this.player.y;
			this.bullets.enableBody = true;
			this.game.physics.arcade.enable(this.bullets);
			this.bullets.anchor.setTo(0.5,0.5);
			this.bullets.body.gravity.y = 600; 
			this.bullets.checkWorldBounds = true;
			this.bullets.outOfBoundsKill = true;
			this.bullets.damage = 100;
			this.bullets.loadTexture('rockBullet');
			this.bullets.revive();
		
			this.game.physics.arcade.moveToPointer(this.bullets, 1200 * this.player.power);
		}
		if(this.player.weapon=='bala'){
			this.bullets.x = this.player.x;
			this.bullets.y = this.player.y;
			this.bullets.enableBody = true;
			this.game.physics.arcade.enable(this.bullets);
			this.bullets.anchor.setTo(0.5,0.5);
			this.bullets.body.gravity.y = 600; 
			this.bullets.checkWorldBounds = true;
			this.bullets.outOfBoundsKill = true;
			this.bullets.damage = 100;
			this.bullets.body.bounce.set(0.5);
			this.bullets.loadTexture('bala');
			this.bullets.revive();
		
			this.game.physics.arcade.moveToPointer(this.bullets, 800 * this.player.power);
		}
		if(this.player.weapon=='canon'){
			this.bullets.x = this.player.x;
			this.bullets.y = this.player.y;
			this.bullets.enableBody = true;
			this.game.physics.arcade.enable(this.bullets);
			this.bullets.anchor.setTo(0.5,0.5);
			this.bullets.body.gravity.y = 600; 
			this.bullets.checkWorldBounds = true;
			this.bullets.outOfBoundsKill = true;
			this.bullets.damage = 100;
			this.bullets.body.bounce.set(0.5);
			this.bullets.loadTexture('canon_bullet');
			this.bullets.revive();
		
			this.game.physics.arcade.moveToPointer(this.bullets, 800 * this.player.power);
		}
		
		this.roundtimer = this.game.time.now;
	},

	addwall(){
		var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
		if(this.player !== null && this.playerHover || this.chooseWeaponBar !== null || this.player.weapon !== 'wall' || this.map[index] !== 0) return;
		this.money -= 10;
		var a = this.game.add.sprite(
			(this.game.input.x + this.camera.view.x) - (this.game.input.x + this.camera.view.x)%28,
			(this.game.input.y+this.camera.view.y) - (this.game.input.y + this.camera.view.y) % 28,
			'stone',
			0,
			this.walls
		);
		this.buildSound.volume = this.game.volumeSound;
		this.buildSound.play();
		a.health = 1;
		this.map[index] = 1;
		this.walls.setAll('body.immovable', true);
		this.walls.setAll('inputEnabled', true);		
		this.walls.setAll('enableBody', true);
		a.events.onInputDown.add(()=>{
			if(this.player.weapon === 'wall'){
				var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
				if(a.health<3){
					a.health+=1;
					if(a.health==3)a.loadTexture('stone_2');
					else if(a.health==2) a.loadTexture('stone_1');

					this.buildSound.volume = this.game.volumeSound;
					this.buildSound.play();
				}
				
				this.money-=10;
				this.map[index]+=1;

			}
		})
	},

	transmit(){
		var index = ((this.game.input.x+this.camera.view.x)-(this.game.input.x+this.camera.view.x)%28)/28 + ((this.game.input.y+this.camera.view.y)-(this.game.input.y+this.camera.view.y)%28)*75/28;
		if(this.playerHover || this.chooseWeaponBar !== null || this.player.weapon !== 'transmission' || this.map[index] !== 0) return;
		var player = this.player;
		var x = (this.game.input.x+this.camera.view.x);
		var y = (this.game.input.y+this.camera.view.y);
		var shrink = this.game.add.tween(player.scale).to( {
			x: 0.3,
			y: 0.3
		 }, 800, Phaser.Easing.Linear.None, true);
		 shrink.onComplete.addOnce(()=>{
			player.x = x;
			player.y = y;
			this.game.add.tween(player.scale).to({
				x: 1,
				y: 1
			}, 500, Phaser.Easing.Linear.None, true);
			this.transmitSound.volume = this.game.volumeSound;
			this.transmitSound.play();
		}, this);
	},

	setupBoomer(boomer) {
        boomer.anchor.x = 0.5;
        boomer.anchor.y = 0.5;
        boomer.animations.add('boom');
    },


	blockParticle(wall) {
        this.emitter.x = wall.x+15;
        this.emitter.y = wall.y+15;
        this.emitter.start(true, 800, null, 15);
	},
	explosion_wall(explosion,wall){
		var j = wall.x/28;
		var i = wall.y/28;	
        if(wall.health == 3){
			wall.health = 2;
			wall.loadTexture('stone_1');
		}
		else if(wall.health === 2){
			wall.health = 1;
			wall.loadTexture('stone');
		}
		
		else {
			var random = this.game.rnd.between(0,100)/100;
			
            if(random<0.3){
				var newcoin = this.game.add.sprite( wall.x+5 , wall.y , 'redcoin', 0, this.coins); 
				this.game.physics.arcade.enable(newcoin);
				newcoin.inputEnabled = true;		
		        newcoin.enableBody = true;

				newcoin.body.gravity.y = 600;
				newcoin.body.bounce.set(0.5);
				newcoin.body.velocity.x = this.game.rnd.between(-200,200);
				newcoin.body.velocity.y = this.game.rnd.between(-400,400);
				//console.log("x="+newcoin.body.velocity.x+" y="+newcoin.body.velocity.y);
				
			}

			this.map[i*75+j] = 0;
			wall.health = 0;
		    var x = this.game.add.tween(wall).to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
			x.onComplete.addOnce(()=>{wall.kill();}, this);
			this.map[i*75+j] = 0;	
		}
	},
	explosion_player(explosion,player){
        player.health-=10;
			// console.log(player.health);
			if(player.health<=0){
				if(this.players_1.getFirstAlive()===null){
					this.txt3.x = 600+this.camera.view.x;
					this.txt3.y = 300+this.camera.view.y;
					this.txt3.text = "PLAYER 2 WIN!!";
					this.txt3.visible = true;
					var aaa = this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
							this.game.paused = false;
							this.game.state.start('singlePlayer');
						}, this);
					aaa.anchor.setTo(0.5, 0.5);
					var bbb = this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
							this.game.paused = false;
							this.game.state.start('menu');
						}, this);
					bbb.anchor.setTo(0.5, 0.5);
						this.game.paused = true;
					}
					else if(this.players_2.getFirstAlive()===null){
						this.txt3.x = 600+this.camera.view.x;
						this.txt3.y = 300+this.camera.view.y;
						this.txt3.text = "PLAYER 1 WIN!!";
						this.txt3.visible = true;
						this.game.add.button(400+this.camera.view.x, 550+this.camera.view.y, 'restart', ()=>{
								this.game.paused = false;
								this.game.state.start('singlePlayer');
							}, this).anchor.setTo(0.5, 0.5);
							this.game.add.button(800+this.camera.view.x, 550+this.camera.view.y, 'exit', ()=>{
									this.game.paused = false;
									this.game.state.start('menu');
								}, this);
								this.game.paused = true;
							}
							player.kill();

				this.dieSound.volume = this.game.volumeSound;
		        this.dieSound.play();
			}
			else{
				this.hurtSound.volume = this.game.volumeSound;
		        this.hurtSound.play();
			}
	},
	bullet_collide_wall(bullet, wall){
		// var flag = 0;
		var j = wall.x/28;
		var i = wall.y/28;	
		if(this.player.weapon=='bala'){
			bullet.body.velocity.x*=0.5;
			if(Math.abs(bullet.body.velocity.x) <0.1){
				var explosion = this.explosions.getFirstExists(false);	
				explosion.scale.setTo(2.5,2.5);			
				explosion.reset(bullet.x-10, bullet.y-10);
				explosion.play('boom', 30, false, true); 
				bullet.kill();
				var quake = this.game.add.tween(this.game.camera).to({x: this.game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 7,true);
                quake.start();
			}
		}
		else if(this.player.weapon=='canon'){

				var explosion = this.explosions.getFirstExists(false);	
				explosion.scale.setTo(1.5,1.5);			
				explosion.reset(bullet.x-10, bullet.y-10);
				explosion.play('boom', 30, false, true); 
				bullet.kill();
				var quake = this.game.add.tween(this.game.camera).to({x: this.game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 7,true);
                quake.start();
			
		}
		if(wall.health == 3&&this.player.weapon!='bala'){
			wall.health = 2;
			wall.loadTexture('stone_1');
		}
		else if(wall.health === 2&&this.player.weapon!='bala'){
			wall.health = 1;
			wall.loadTexture('stone');
		}
		
		else if(this.player.weapon!='bala'){
			var random = this.game.rnd.between(0,100)/100;
			
            if(random<1){
				var newcoin = this.game.add.sprite( wall.x+5 , wall.y , 'redcoin', 0, this.coins); 
				this.game.physics.arcade.enable(newcoin);
				newcoin.inputEnabled = true;		
		        newcoin.enableBody = true;

				newcoin.body.gravity.y = 600;
				newcoin.body.bounce.set(0.5);
				newcoin.body.velocity.x = this.game.rnd.between(-200,200);
				newcoin.body.velocity.y = this.game.rnd.between(-400,400);
				//console.log("x="+newcoin.body.velocity.x+" y="+newcoin.body.velocity.y);
				
			}

			this.map[i*75+j] = 0;
			wall.health = 0;
		    var x = this.game.add.tween(wall).to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
		    x.onComplete.addOnce(()=>{wall.kill();}, this);	
		}
		if(this.player.weapon !== 'bala')bullet.kill();
		// console.log(bullet.alive);
		// console.log(this.bullets.alive);
		this.blockParticle(wall);		
		            
		 //flag = 1;
		this.rockHitSound.volume = this.game.volumeSound;
		this.rockHitSound.play();
			
	},

	bullet_collide_player(bullet, player){
		// var flag = 0;
		if(this.player.weapon=='bala' && player!=this.player){
			bullet.body.velocity.x*=0.5;
			if(Math.abs(bullet.body.velocity.x) <0.1){
				var explosion = this.explosions.getFirstExists(false);	
				explosion.scale.setTo(2.5,2.5);			
				explosion.reset(bullet.x-10, bullet.y-10);
				explosion.play('boom', 30, false, true); 
				bullet.kill();
			}
		}
		else if(this.player.weapon=='canon' && player!=this.player){

				var explosion = this.explosions.getFirstExists(false);	
				explosion.scale.setTo(1.5,1.5);			
				explosion.reset(bullet.x-10, bullet.y-10);
				explosion.play('boom', 30, false, true); 
				bullet.kill();
			
		}
		else if(player!==this.player && this.player.weapon!='bala'){
			bullet.kill();
			player.health-=bullet.damage;
			// console.log(player.health);
			if(player.health<=0){
				player.kill();

				this.dieSound.volume = this.game.volumeSound;
		        this.dieSound.play();
			}
			else{
				this.hurtSound.volume = this.game.volumeSound;
		        this.hurtSound.play();
			}
		}
	},
	coin_collide_player1(coin, player){

			coin.kill();
			this.money+=50;

			this.collectMoneySound.volume = this.game.volumeSound;
		this.collectMoneySound.play();

	},
	coin_collide_player2(coin, player){

		coin.kill();
		this.money+=50;

		this.collectMoneySound.volume = this.game.volumeSound;
		this.collectMoneySound.play();
	},
	coin_collide_wall(coin,wall){
	   coin.body.velocity.x*=0.5;
	},

	land(){
		this.player.jumping = false;
	},
	
	movePlayer() {
		if ((this.cursor.up.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.W)) && !this.player.jumping) { 
            if(this.cursor.left.isDown) this.player.animations.play('left');   
            else if(this.cursor.right.isDown) this.player.animations.play('right');
            else if(!this.cursor.left.isDown && !this.cursor.right.isDown) this.player.animations.play('straight');
			
			this.player.body.velocity.y = -350;
			this.player.jumping = true;
			this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
			// this.camera.focusOnXY(this.player.x, this.player.y);
			// this.game.add.tween(this.camera).to({x: this.player.x - 600, y: this.player.y - 350}, 500).start();
		}  
		
		//player go left
        else if (this.cursor.left.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
			//this.player.x -=1.5;
			this.player.body.velocity.x = -100;
			this.player.animations.play('left');
			this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
			// this.camera.focusOnXY(this.player.x, this.player.y);
			// this.game.add.tween(this.camera).to({x: this.player.x - 600, y: this.player.y - 350}, 500).start();
		}
		
		//player go right
        else if (this.cursor.right.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.D)) { 
			//this.player.x += 1.5;
			this.player.body.velocity.x = 100;
			this.player.animations.play('right');
			this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
			// this.camera.focusOnXY(this.player.x, this.player.y);
			// this.game.add.tween(this.camera).to({x: this.player.x - 600, y: this.player.y - 350}, 500).start();
		}
		
		//player stand still
        else if(this.player !== null){
            this.player.body.velocity.x = 0;
			this.player.animations.play('straight');
			if(this.bullets.alive === true){
				this.game.camera.follow(this.bullets, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
			}
			else if (this.game.input.mousePointer.isDown && this.shoot && !this.player.input.pointerOver() && !this.bullets.alive && this.chooseWeaponBar === null){
				this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
			}
			else {
				if(this.player.body.velocity.x !== 0 || this.player.body.velocity.y !== 0){
					
					this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT, 0.1, 0.1);
					
					// this.camera.focusOnXY(this.player.x, this.player.y);
					// this.game.add.tween(this.camera).to({x: this.player.x - 600, y: this.player.y - 350}, 500).start();
				}
				else {
					this.game.camera.target = null;
				}
			}
        } 
	},

	play_animations(){
		if (this.player !== null && !this.player.body.touching.down) { 
			if(this.cursor.left.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.A))this.player.scale.x = -1;
			else if(this.cursor.right.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.D))this.player.scale.x = 1;
			this.player.animations.play('jump');
			
			// this.camera.focusOnXY(this.player.x, this.player.y);
			// this.game.add.tween(this.camera).to({x: this.player.x - 600, y: this.player.y - 350}, 500).start();
		}  
		
		//player go left
        else if (this.cursor.left.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
			
			this.player.scale.x = -1;
			this.player.animations.play('walk');
	
		}
		
		//player go right
        else if (this.player !== null && this.cursor.right.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.D)) { 
			
			this.player.scale.x = 1;
			this.player.animations.play('walk');
			
		}
		
		//player stand still
        else if(this.player !== null) {
			this.player.animations.play('idle');
           
        } 
	}

}
