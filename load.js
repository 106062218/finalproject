var Load = {
	preload(){
		var loadingLabel = this.game.add.text(this.game.width/2, 150, 'loading...', {
			font: '30px font',
			fill: '#ffffff'
		});
		loadingLabel.anchor.setTo(0.5, 0.5);

		this.game.stage.backgroundColor = '#ffb915';
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.renderer.renderSession.roundPixels = true;

		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;

		this.game.load.spritesheet('player', './assets/player_2_1.png', 46.5, 73);
		this.game.load.spritesheet('player_1', './assets/player_1_1.png', 46.5, 73);
		this.game.load.image('arrow', 'assets/arrow.png');
		this.game.load.image('bullet', 'assets/bullet.png');
		this.game.load.image('ground', 'assets/ground.jpg');
		this.game.load.image('drag_u', './assets/drag_u.png');
		this.game.load.image('drag_d', './assets/drag_d.png');
		this.game.load.image('drag_r', './assets/drag_r.png');
		this.game.load.image('drag_l', './assets/drag_l.png');
		this.game.load.image('pauseBtn', './assets/pause.png');
		this.game.load.image('board', './assets/pause-board.png');
		this.game.load.image('resume', './assets/resume.png');
		this.game.load.image('restart', './assets/restart.png');
		this.game.load.image('options', './assets/options.png');
		this.game.load.image('exit', './assets/exit.png');
		// this.game.load.image('stone', './assets/stone.jpg');
		this.game.load.image('stone', './assets/3.png');
		// this.game.load.image('stone_1', './assets/stone_1.jpg');
		this.game.load.image('stone_1', './assets/2.png');
		// this.game.load.image('stone_2', './assets/stone_2.jpg');
		this.game.load.image('stone_2', './assets/1.png');
		this.game.load.image('stone_2_1', './assets/11.png');
		this.game.load.image('stone_2_2', './assets/111.png');
		this.game.load.image('pixel', './assets/pixel.png');
		this.game.load.image('gun', './assets/gun.png');
		this.game.load.image('sling', './assets/sling.png');
		this.game.load.image('chooseWeaponBar', './assets/asdsa.jpg');
		this.game.load.image('rockBullet', './assets/rock_bullet.png');

		this.game.load.spritesheet('boom','assets/explosion.png',64,64);

		this.game.load.image('transparentBg', './assets/transparentBg.png');
		this.game.load.image('transparentBg_1', './assets/transparentBg_1.png');
		this.game.load.image('transparentBg_2', './assets/transparentBg_2.png');
		this.game.load.image('transmissionWeapon', './assets/transmissionWeapon.png');
		this.game.load.image('transmissionCursor', './assets/transmissionCursor.png');
		this.game.load.image('transmissionIcon', './assets/transmissionIcon.png');
		this.game.load.image('background','./assets/game_background.jpg');

		this.game.load.image('rockIcon', './assets/rock_icon.png');
		this.game.load.image('slingIcon', './assets/sling_icon.png');
		this.game.load.image('buildIcon', './assets/buildIcon.png');

		this.game.load.image('redcoin', './assets/red_coin.png');
		this.game.load.image('greencoin', './assets/green_coin.png');
		this.game.load.image('bluecoin', './assets/blue_coin.png');

		this.game.load.audio('build', './sounds/build.wav');
		this.game.load.audio('collectMoney', './sounds/collectMoney.wav');
		this.game.load.audio('die', './sounds/die.wav');
		this.game.load.audio('hover', './sounds/hover.wav');
		this.game.load.audio('hurt', './sounds/hurt.ogg');
		this.game.load.audio('rockHit', './sounds/rockHit.mp3');
		this.game.load.audio('sling', './sounds/sling.mp3');
		this.game.load.audio('sling_2', './sounds/sling_2.wav');
		this.game.load.audio('transmit', './sounds/transmit.wav');

		this.game.load.audio('menuMusic', './sounds/menuBgMusic.mp3');
		this.game.load.audio('gameMusic', './sounds/gameBgMusic.mp3');

		this.game.load.image('health_in', './assets/health_in.png');
		this.game.load.image('health_out', './assets/health_out.png');
		this.game.load.image('pointer', './assets/pointer.png');
		this.game.load.image('bala','./assets/bala.png');
		this.game.load.image('balaIcon','./assets/balaicon.png');

		this.game.load.image('canon','./assets/canon.png');
		this.game.load.image('canonIcon','./assets/canonIcon.png');
		this.game.load.image('canon_bullet','./assets/canon_bullet.png');

		this.game.load.image('meatIcon','./assets/meat.png');
		this.game.load.image('moneyIcon','./assets/money.png');
		this.game.load.image('timerIcon','./assets/timerIcon.png');


		this.game.load.image('background1','./assets/game_background1.jpg');
		this.game.load.image('background2','./assets/game_background2.jpg');
		this.game.load.image('background3','./assets/game_background3.jpg');
		this.game.load.image('background4','./assets/game_background4.jpg');

		this.game.load.image('menuBg', './assets/menuBg.jpg');
		this.game.load.image('settingBg', './assets/settingBg.jpg');
	},

	create(){
		this.game.state.start('menu');

		this.game.menuMusic = null;
		this.game.gameMusic = null;
		this.game.volumeMusic = 1;
		this.game.volumeSound = 1;
	}
}